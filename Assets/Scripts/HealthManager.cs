﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour {

	public static int playerHealth;
	public int maxPlayerHealth;

	public PlayerController player;
	public GameObject gameOverScreen;

	public bool isDead;
	
	Text text;

	private LevelManager levelManager;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();

		playerHealth = maxPlayerHealth;

		levelManager = FindObjectOfType<LevelManager> ();
		isDead = false;
	}
	
	// Update is called once per frame
	void Update () {
	    if (playerHealth <= 0 && !isDead) {
			playerHealth = 0;
			levelManager.RespawnPlayer();
			isDead = true;
		}

		text.text = "" + playerHealth;
	}

	public static void HurtPlayer(int damageToGive)
	{
		playerHealth -= damageToGive;
	}

	public void FullHealth()
	{
		playerHealth = maxPlayerHealth;
	}

	public void DisableStartScreen()
	{
		if (playerHealth < 0) 
		{
			player.gameObject.SetActive(false);
			gameOverScreen.SetActive(true);
		}
	}
}
