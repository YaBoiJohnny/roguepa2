﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float moveSpeed;
	private float moveVelocity;
	public float jumpHeight;

	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;
	private bool grounded;

	private bool doubleJumped;

	private Animator anim;

	public float shotDelay;
	private float shotDelayCounter;

	public float knockback;
	public float knockbackCount;
	public float knockbackLength;
	public bool knockFromRight;

	public Transform firePoint;
	public GameObject ninjaStar;

	private Rigidbody2D myrigidbody2D;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();

		myrigidbody2D = GetComponent<Rigidbody2D> ();
	}

	void FixedUpdate(){
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);
	
	}


	// Update is called once per frame
	void Update () {

		if (grounded)
			doubleJumped = false;

		anim.SetBool ("Grounded", grounded);

		if(Input.GetKeyDown (KeyCode.Space) && grounded)
		{
			//GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpHeight);
			Jump ();
		}

		if (Input.GetKeyDown (KeyCode.Space) && !doubleJumped && !grounded) {
			//GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpHeight);
			Jump ();
			doubleJumped = true;
		}

		moveVelocity = 0f;

		if(Input.GetKey (KeyCode.D))
		{
			//GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
			moveVelocity = moveSpeed;
		}

		if(Input.GetKey (KeyCode.A))
		{
			//GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
			moveVelocity = -moveSpeed;
		}
		if (knockbackCount <= 0) {
			myrigidbody2D.velocity = new Vector2 (moveVelocity, myrigidbody2D.velocity.y);
		} else {
		   if(knockFromRight)
				myrigidbody2D.velocity = new Vector2 (-knockback, knockback);
			if(!knockFromRight)
				myrigidbody2D.velocity = new Vector2 (knockback, knockback);
			knockbackCount -= Time.deltaTime;

		}



		anim.SetFloat ("Speed",Mathf.Abs(myrigidbody2D.velocity.x));

		if (myrigidbody2D.velocity.x > 0)
			transform.localScale = new Vector3 (1f, 1f, 1f);
		else if (myrigidbody2D.velocity.x < 0)
			transform.localScale = new Vector3 (-1f, 1f, 1f);

		if (Input.GetKeyDown (KeyCode.Return)) {
			Instantiate(ninjaStar, firePoint.position, firePoint.rotation);
			shotDelayCounter = shotDelay;
		}

		if (Input.GetKey(KeyCode.Return)) {
			shotDelayCounter -= Time.deltaTime;

			if(shotDelayCounter <=0)
			{
				shotDelayCounter = shotDelay;
				Instantiate(ninjaStar, firePoint.position, firePoint.rotation);

			}
		}

		if (anim.GetBool ("Sword"))
			anim.SetBool ("Sword", false); 

		if (Input.GetKey (KeyCode.L)) {
			anim.SetBool("Sword", true);
		}
	}


	public void Jump()
	{
		myrigidbody2D.velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpHeight);
	}                                                                                       
 }
